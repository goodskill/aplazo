import { ActionReducerMap } from "@ngrx/store";
import * as itemsReducer from "./app/components/info.reducer";

export interface AppState {
    data: itemsReducer.State
}


export const AppReducers: ActionReducerMap<AppState> = {
    data: itemsReducer.itemsReducer
}