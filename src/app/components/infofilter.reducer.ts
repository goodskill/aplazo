import { createReducer, on } from "@ngrx/store";
import { setCharacter, setLocation, setEpisode } from "./infofilter.actions";

export interface State{
    character: Object,
    location: Object,
    Episode: Object
}
export const initialState: State = {
    character: Object,
    location: Object,
    Episode: Object
}



const _infoReducer = createReducer(initialState,
        on(setCharacter, (state, {character}) => ({ ...state, characters: {...character}})),
        on(setEpisode, (state, {episode}) => ({ ...state, Episodes: {...episode}})),
        on(setLocation, (state, {location}) => ({ ...state, locations: {...location}})),
    );

    export function itemsReducer(state:any, action:any){
        return _infoReducer(state, action)
    }
