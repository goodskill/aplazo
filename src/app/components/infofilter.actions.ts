import { createAction, props } from "@ngrx/store";

export const loadCharacter = createAction(
    "[Character] load Character",
    props<{id:string}>
    );
export const setCharacter = createAction(
        "[Character] set Character",
        props<{character:Object}>()
    );
export const setCharacterError = createAction(
        "[Character] Load Character Error",
        props<{characterError:Object}>()
    );



export const loadLocation = createAction("[Location] load Location", props<{id:string}>);
export const setLocation = createAction(
        "[Location] set Location",
        props<{location:Object}>()
    );
export const setLocationError = createAction(
    "[Location] Load Location Error",
    props<{locationError:Object}>()
);


export const loadEpisode = createAction("[Episode] load Episode", props<{id:string}>);
export const setEpisode = createAction(
        "[Episode] set Episode",
        props<{episode:Object}>()
    );
    export const setEpisodeError = createAction(
        "[Episode] Load Episode Error",
        props<{episodeError:Object}>()
    );
