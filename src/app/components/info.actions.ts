import { createAction, props } from "@ngrx/store";

export const loadCharacter = createAction("[Characters] load Characters");
export const setCharacters = createAction(
        "[Characters] set Characters",
        props<{characters:Object}>()
    );
export const setCharactersError = createAction(
        "[Character] Load Character Error",
        props<{characterError:Object}>()
    );



export const loadLocations = createAction("[Locations] load Locations");
export const setLocations = createAction(
        "[Locations] set Locations",
        props<{locations:Object}>()
    );
export const setLocationsError = createAction(
    "[Location] Load Locations Error",
    props<{locationError:Object}>()
);


export const loadEpisodes = createAction("[Episodes] load Episodes");
export const setEpisodes = createAction(
        "[Episodes] set Episodes",
        props<{episodes:Object}>()
    );
    export const setEpisodesError = createAction(
        "[Episode] Load Episodes Error",
        props<{episodeError:Object}>()
    );
