import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';
import * as dataActions from '../info.actions'

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {
  locations:any;
  location:any;
  showSpinnerModal = true;
  constructor(public api: RickandmortyService, private store: Store<any>) {
  }

  ngOnInit(): void {
    this.store.dispatch(dataActions.loadLocations())
    this.store.select('data').subscribe(data =>{
      this.locations = data.locations
    })
  }
  seeDetails(id:any){
    this.showSpinnerModal = true;
    this.api.getSingleLocation(id).subscribe(data =>{
      this.showSpinnerModal = false;
      this.location = data;
    })
  }
  filterUp(data:string){
    this.api.filterLocation(data).subscribe(data=>{
    
    })
  }


}
