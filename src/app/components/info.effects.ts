import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, mergeMap, tap } from "rxjs/operators";
import { RickandmortyService } from "../services/rickandmorty.service";
import * as infoAction from './info.actions'

@Injectable()
export class CharacterEffects {
    constructor(private action$: Actions, private api:RickandmortyService){}
    loadChracter$ = createEffect(
        () => this.action$.pipe(
            ofType(infoAction.loadCharacter),
            // tap(data => console.log("Hola hola", data)),
            mergeMap(
                () => this.api.getCharacter(1).pipe(
                    map(characters => infoAction.setCharacters({characters:characters})),
                    // tap( data => console.log("Effects Data", data)
                    catchError( err => of (infoAction.setCharactersError({characterError:err})))
                    )
                )
            )
        )
    // );
}
@Injectable()
export class LocationsEffects {
    constructor(private action$: Actions, private api:RickandmortyService){}
    loadLocation$ = createEffect(
        () => this.action$.pipe(
            ofType(infoAction.loadLocations),
            // tap(data => console.log("Hola hola", data)),
            mergeMap(
                () => this.api.getLocation(1).pipe(
                    map(locations => infoAction.setLocations({locations:locations})),
                    // tap( data => console.log("Effects Data", data)
                    catchError( err => of (infoAction.setLocationsError({locationError:err})))
                    )
                )
            )
        )
    // );
}


@Injectable()
export class EpisodesEffects {
    constructor(private action$: Actions, private api:RickandmortyService){}
    loadEpisode$ = createEffect(
        () => this.action$.pipe(
            ofType(infoAction.loadEpisodes),
            // tap(data => console.log("Hola hola", data)),
            mergeMap(
                () => this.api.getEpisode(1).pipe(
                    map(episodes => infoAction.setEpisodes({episodes:episodes})),
                    // tap( data => console.log("Effects Data", data)
                    catchError( err => of (infoAction.setEpisodesError({episodeError:err})))
                    )
                )
            )
        )
    // );
}