import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  formGroup:FormGroup;
  @Output() name = new EventEmitter<string>();
  constructor() {
    this.formGroup = new FormGroup({
      name: new FormControl("")
    })
  }

  ngOnInit(): void {
  }

  public onSubmit2(){
    this.name.emit(this.formGroup.value.name)
  }

}
