import { CharacterEffects, EpisodesEffects, LocationsEffects } from "./info.effects";

export const EffectsArray: any = [CharacterEffects, LocationsEffects, EpisodesEffects];
