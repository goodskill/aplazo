import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';

@Component({
  selector: 'app-characters-detail',
  templateUrl: './characters-detail.component.html',
  styleUrls: ['./characters-detail.component.css']
})
export class CharactersDetailComponent implements OnInit {
  id:any;
  character:any;
  showSpinner = true;
  constructor(public api: RickandmortyService, private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
    this.getInfoCharacter();
  }
  getInfoCharacter(){
    this.api.getInfoCharacter(this.id).subscribe(data => {
      this.character = data;
      this.showSpinner = false;
    })
  }

}
