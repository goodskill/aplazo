import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';
import { loadCharacter } from '../info.actions'

import * as dataActions from '../info.actions'
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  characters:any;



  constructor(public api: RickandmortyService, private store: Store<any>, private route: Router) {
  }

  ngOnInit(): void {    
    this.store.dispatch(loadCharacter())
    this.store.select('data').subscribe(data =>{
      this.characters = data.characters
    })
  }

  seeDetails(id:number){
    this.route.navigateByUrl("home/"+id);
  }

  filterUp(data:string){
    this.api.filterCharacter(data).subscribe(data => {
      console.log(data);
    });
  }

}
