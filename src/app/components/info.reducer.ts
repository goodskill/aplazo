import { createReducer, on } from "@ngrx/store";
import { setCharacters, setLocations, setEpisodes } from "./info.actions";

export interface State{
    characters: Object,
    locations: Object,
    Episodes: Object
}
export const initialState: State = {
    characters: Object,
    locations: Object,
    Episodes: Object
}



const _infoReducer = createReducer(initialState,
        on(setCharacters, (state, {characters}) => ({ ...state, characters: {...characters}})),
        on(setEpisodes, (state, {episodes}) => ({ ...state, Episodes: {...episodes}})),
        on(setLocations, (state, {locations}) => ({ ...state, locations: {...locations}})),
    );

    export function itemsReducer(state:any, action:any){
        return _infoReducer(state, action)
    }
