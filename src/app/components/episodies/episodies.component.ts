import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';

import * as dataActions from '../info.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-episodies',
  templateUrl: './episodies.component.html',
  styleUrls: ['./episodies.component.css']
})
export class EpisodiesComponent implements OnInit {
  episodes:any;
  episode:any;
  showSpinnerModal = true;

  constructor(public api: RickandmortyService, private store: Store<any>) {

  }

  ngOnInit(): void {
    this.store.dispatch(dataActions.loadEpisodes())
    this.store.select('data').subscribe(data =>{
      this.episodes = data.Episodes
    })
  }

  seeDetails(id:any){
    this.showSpinnerModal = true;
    this.api.getSingleEpisode(id).subscribe(data => {
      this.episode = data;
      this.showSpinnerModal = false;
    });
  }
  filterUp(data:string){
    this.api.filterEpisode(data).subscribe(data => {
    })
  }
}
