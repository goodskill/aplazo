import { Injectable } from "@angular/core";
import { dataPaginator } from "../class/classes";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RickandmortyService {
  private api = "https://rickandmortyapi.com/api/"

  // character
  // location
  // episode
  constructor(private http: HttpClient) { }

  public getCharacter(page:number): Observable<any> {
    return this.http.get<any>(`${this.api}character/?page${page}`)
  }
  public getLocation(page:number): Observable<any> {
    return this.http.get<any>(`${this.api}location/?page${page}`);
  }
  public getEpisode(page:number): Observable<any> {
    return this.http.get<any>(`${this.api}episode/?page${page}`);
  }

  public getSingleLocation(id:number):Observable<any>{
    return this.http.get<any>(`${this.api}location/${id}`);
  }
  public getSingleEpisode(id:number):Observable<any>{
    return this.http.get<any>(`${this.api}episode/${id}`);
  }
  public getInfoCharacter(id:number):Observable<any>{
    return this.http.get<any>(`${this.api}character/${id}`)
  }

  public filterCharacter(name:string):Observable<any>{
    return this.http.get<any>(`${this.api}character/?name=${name}`);
  }
  public filterEpisode(name:string):Observable<any>{
    return this.http.get<any>(`${this.api}episode/?name=${name}`);
  }
  public filterLocation(name:string):Observable<any>{
    return this.http.get<any>(`${this.api}location/?name=${name}`);
  }
}
