import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EpisodiesComponent } from './components/episodies/episodies.component';
import { CharactersDetailComponent } from './components/home/characters-detail/characters-detail.component';
import { HomeComponent } from './components/home/home.component';
import { LocationsComponent } from './components/locations/locations.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home/:id', component:CharactersDetailComponent},
  {path: 'episodies', component:EpisodiesComponent},
  {path: 'locations', component:LocationsComponent},
  {path: '**', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
